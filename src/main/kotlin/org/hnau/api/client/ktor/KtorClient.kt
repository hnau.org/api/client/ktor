package org.hnau.api.client.ktor

import org.hnau.api.client.common.ApiClientEngine
import io.ktor.client.HttpClient
import io.ktor.client.call.call
import io.ktor.client.request.post
import org.hnau.api.client.common.ApiCall
import org.hnau.base.data.bytes.provider.BufferedBytesProvider
import org.hnau.base.data.bytes.receiver.BufferedBytesReceiver
import org.hnau.base.extensions.asHexToBytes
import org.hnau.base.extensions.toHexString


data class KtorClientInfo(
        val host: String,
        val port: Int,
        val path: String = "/"
)

fun KtorClient(
        info: KtorClientInfo
) = object : ApiClientEngine {

    private val path = info.run { "$host:$port$path" }

    override suspend fun <O> call(
            call: ApiCall<O>
    ) = HttpClient().use { client ->
        client
                .post<String>(path) {
                    val requestBytesReceiver = BufferedBytesReceiver()
                    call.writeRequest(requestBytesReceiver)
                    body = requestBytesReceiver.buffer.toHexString()
                }
                .let { responseString ->
                    val responseBytes = responseString.asHexToBytes()
                    val responseBytesProvider = BufferedBytesProvider(responseBytes)
                    call.readResponse(responseBytesProvider)
                }
    }

}